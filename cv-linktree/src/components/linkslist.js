import hog from '../asset/images/hog.svg';
import tarot from '../asset/images/tarot.svg';
import createx from '../asset/images/createx.svg';
import beeline from '../asset/images/iconBee.png';

const linktree = [
    {
        ava: tarot,
        name: <h3 className='link__header'>Tarot Cards</h3>,
        link: 'tarot.udogcv.site',
        key: '666',
    },
    {
        name: <h3 className='link__header'>On-line Edu</h3>,
        link: 'online.udogcv.site',
        key: '09',
    },
    {
        ava: hog,
        name: <h3 className='link__header'>House of<br /> Gaming</h3>,
        link: 'hog.udogcv.site',
        key: '08',
    },
    {
        name: <h3 className='link__header'>Adept</h3>,
        link: 'ad.udogcv.site',
        key: '07',
    },
    {
        name: <h3 className='link__header'>Currencies<br />3205</h3>,
        link: 'curr.udogcv.site',
        key: '05',
    },
    {
        name: <h3 className='link__header'>Welbex</h3>,
        link: 'wel.udogcv.site',
        key: '04',
    },
    {
        ava: createx,
        name: <h3 className='link__header'>CreateX<br />Construction</h3>,
        link: 'createx.udogcv.site',
        key: '03',
    },
    {
        ava: beeline,
        name: <h3 className='link__header'>Beeinterns<br />2022</h3>,
        link: 'beeint.udogcv.site',
        key: '02',
    },
    {
        ava: beeline,
        name: <h3 className='link__header'>Project<br />First</h3>,
        link: 'pr1st.udogcv.site',
        key: '01',
    },
];

export default linktree;