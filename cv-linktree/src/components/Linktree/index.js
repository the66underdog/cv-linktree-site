import { useEffect, useState } from "react";
import linktree from "../linkslist";
import './style.css';
import reactLogo from '../../asset/images/logo.svg';

function Linktree() {

    // eslint-disable-next-line no-unused-vars
    const [links, setLinks] = useState(linktree);

    useEffect(() => {
        for (let item of document.getElementsByClassName('link__header')) {
            item.style.width = item.scrollTo();
        }
    }, []);

    return (
        <div className="main__container">
            <div className="content main__content">
                <img className="content__bg-image" src={reactLogo} alt='React logo' />
                <img className="content__bg-image" src={reactLogo} alt='React logo' />
                <img className="content__bg-image" src={reactLogo} alt='React logo' />
                <ul className='linktree'>
                    {links.length && links.map((item) => (
                        <li key={item.key} className="linktree__item">
                            <a href={`http://${item.link}`} className="link">
                                <div className="link__ava-container">
                                    {item.ava ? <img className="link__ava" alt="ava" src={item.ava} /> : null}
                                </div>
                                <div className="link__text-container">
                                    {item.name}
                                    <span className="link__uri">
                                        {item.link}
                                    </span>
                                </div>
                            </a>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}

export default Linktree;